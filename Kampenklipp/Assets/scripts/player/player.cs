using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    //Variabeln

    public float maxHealth, MaxThrist, maxHunger;
    public float thristIncreseRate, hungerincreseRate;
    private float health, thrist, hunger;
    
    public bool dead;
    //Funktionen

    public void Start()
    {
        health = maxHealth;
    }

    public void Update()
    {
        //Durst
        if (!dead)
        {
            thrist += thristIncreseRate * Time.deltaTime;
            hunger += hungerincreseRate * Time.deltaTime;
        }
        if (thrist >= MaxThrist)
            Die();
        if (hunger >= maxHunger)
            Die();
    }
public void Die()
    {
        dead = true;
        print("Du bist Tot :(");
    }
}
